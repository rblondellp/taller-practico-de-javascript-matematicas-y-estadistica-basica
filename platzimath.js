const PlatziMath = {};

PlatziMath.esPar = function esPar(lista) {
    return lista.length % 2 == 0;
};

PlatziMath.esImpar = function esImpar(lista) {
    return lista.length % 2;
};

PlatziMath.calcularModa = function calcularModa(lista) {
    const listaCount = {};

    for (let i = 0; i < lista.length; i++) {
        const elemento = lista[i];

        if (listaCount[elemento]) {
            listaCount[elemento] += 1;
        } else {
            listaCount[elemento] = 1;
        }
    }

    const listaArray = Object.entries(listaCount);
    const listaOrdenada = PlatziMath.ordenarListaBidimencional(listaArray, 1);
    const listaOrdenadaMaxNumber = listaOrdenada[listaOrdenada.length - 1];
    const moda = listaOrdenadaMaxNumber[0];
    // console.log({
    //     listaCount,
    //     listaArray,
    //     listaOrdenada,
    //     listaOrdenadaMaxNumber,
    // });

    // console.log("La moda es: " + listaOrdenadaMaxNumber[0]);
    return moda;
};

PlatziMath.calcularMediana = function calcularMediana(listaDesordenada) {
    const lista = PlatziMath.ordenarLista(listaDesordenada);
    const listaEsPar = PlatziMath.esPar(lista);

    if (listaEsPar) {
        const mitad1ListaPar = lista[lista.length / 2 - 1];
        const mitad2ListaPar = lista[lista.length / 2];
        const listaMitades = [mitad1ListaPar, mitad2ListaPar];
        const medianaListaPar = PlatziMath.calcularPromedio(listaMitades);
        return medianaListaPar;
    } else {
        const indexMitadListaImpar = Math.floor(lista.length / 2);
        const medianaListaImpar = lista[indexMitadListaImpar];
        return medianaListaImpar;
    }
};

PlatziMath.calcularPromedio = function calcularPromedio(lista) {
    const sumarTodosElementos = (valorAcumulado, nuevoValor) =>
        valorAcumulado + nuevoValor;
    const sumaLista = lista.reduce(sumarTodosElementos);

    const promedio = sumaLista / lista.length;
    return promedio;
};

PlatziMath.ordenarLista = function ordenarLista(listaDesordenada) {
    function ordenarListaSort(valorAcumulado, nuevoValor) {
        return valorAcumulado - nuevoValor;
    }

    const lista = listaDesordenada.sort(ordenarListaSort);
    return lista;
};

PlatziMath.ordenarListaBidimencional = function ordenarListaBidimencional(
    listaDesordenada
) {
    function ordenarListaSort(valorAcumulado, nuevoValor) {
        return valorAcumulado[1] - nuevoValor[1];
    }

    const lista = listaDesordenada.sort(ordenarListaSort);
    return lista;
};
