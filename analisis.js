//Analisis personal
function encontrarPersona(personaEnBusqueda) {
    return salarios.find(persona => persona.name === personaEnBusqueda);
}

function medianaPorPersona(nombrePersona) {
    const trabajos = encontrarPersona(nombrePersona).trabajos;
    const salarios = trabajos.map(el => el.salario);
    const medianaSalarios = PlatziMath.calcularMediana(salarios);
    return medianaSalarios;
}

function proyeccionPorPersona(nombrePersona) {
    const trabajos = encontrarPersona(nombrePersona).trabajos;
    let porcentajesCrecimiento = [];

    for (let index = 1; index < trabajos.length; index++) {
        const salarioActual = trabajos[index].salario;
        const salarioAnterior = trabajos[index - 1].salario;
        const crecimiento = salarioActual - salarioAnterior;
        const procentajeCrecimiento = crecimiento / salarioAnterior;
        porcentajesCrecimiento.push(procentajeCrecimiento);
    }

    const medianaPorcentajesCrecimiento = PlatziMath.calcularMediana(
        porcentajesCrecimiento
    );

    const ultimoSalario = trabajos[trabajos.length - 1].salario;
    const aumento = ultimoSalario * medianaPorcentajesCrecimiento;
    const nuevoSalario = ultimoSalario + aumento;
    return nuevoSalario;
}

//Análisis empresarial
const empresas = {};
salarios.forEach(persona => {
    persona.trabajos.forEach(trabajo => {
        if (!empresas[trabajo.empresa]) {
            empresas[trabajo.empresa] = {};
        }

        if (!empresas[trabajo.empresa][trabajo.year]) {
            empresas[trabajo.empresa][trabajo.year] = [];
        }

        empresas[trabajo.empresa][trabajo.year].push(trabajo.salario);
    });
});

function medianaEmpresaYear(nombre, year) {
    if (!empresas[nombre]) return console.warn("La empresa no existe");
    if (!empresas[nombre][year])
        return console.warn("La empresa no dio salarios ese año");

    return PlatziMath.calcularMediana(empresas[nombre][year]);
}

function proyeccionPorEmpresa(nombre) {
    if (!empresas[nombre]) return console.warn("La empresa no existe");

    const empresaYear = Object.keys(empresas[nombre]);
    const listaMedianaYears = empresaYear.map(year =>
        medianaEmpresaYear(nombre, year)
    );

    let porcentajesCrecimiento = [];
    for (let index = 1; index < listaMedianaYears.length; index++) {
        const salarioActual = listaMedianaYears[index];
        const salarioAnterior = listaMedianaYears[index - 1];
        const crecimiento = salarioActual - salarioAnterior;
        const procentajeCrecimiento = crecimiento / salarioAnterior;
        porcentajesCrecimiento.push(procentajeCrecimiento);
    }

    const medianaPorcentajesCrecimiento = PlatziMath.calcularMediana(
        porcentajesCrecimiento
    );

    const ultimaMediana = listaMedianaYears[listaMedianaYears.length - 1];
    const aumento = ultimaMediana * medianaPorcentajesCrecimiento;
    const nuevaMediana = ultimaMediana + aumento;
    return nuevaMediana;
}

//Analisis general
function medianaGeneral() {
    const listaMedianas = salarios.map(persona =>
        medianaPorPersona(persona.name)
    );
    const mediana = PlatziMath.calcularMediana(listaMedianas);
    return mediana;
}

function medianaTop10() {
    const listaMedianas = salarios.map(persona =>
        medianaPorPersona(persona.name)
    );
    const medianasOrdenadas = PlatziMath.ordenarLista(listaMedianas);

    const cantidad = listaMedianas.length / 10;
    const limite = listaMedianas.length - cantidad;

    const top10 = medianasOrdenadas.slice(limite, medianasOrdenadas.length);

    const medianaTop10 = PlatziMath.calcularMediana(top10);
    return medianaTop10;
}
