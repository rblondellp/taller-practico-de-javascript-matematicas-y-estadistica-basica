const btn = document.getElementById("calcular");
const inputPrice = document.getElementById("price");
const inputCoupon = document.getElementById("coupon");
const pResult = document.getElementById("result");

btn.addEventListener("click", calcularPrecioConDescuento);

function calcularPrecioConDescuento() {
    // const couponObj = {
    //     Raycupon: 30,
    //     este_es_otro_cupon: 25,
    // };

    const couponsList = [];
    couponsList.push({
        name: "Raycupon",
        discount: 30,
    });
    couponsList.push({
        name: "este_es_otro_cupon",
        discount: 25,
    });

    const precio = parseInt(inputPrice.value);
    const cupon = inputCoupon.value;

    if (!precio || !cupon)
        return (pResult.innerText = "Por favor llena el formulario");

    let discount;

    function isCouponInArray(couponElement) {
        return couponElement.name == cupon;
    }

    const couponInArray = couponsList.find(isCouponInArray);

    if (couponInArray) {
        discount = couponInArray.discount;
    } else {
        return (pResult.innerText = "El cupon no es valido");
    }

    // if (couponObj[inputCoupon.value]) {
    //     discount = couponObj[inputCoupon.value];
    // } else {
    //     return (pResult.innerText = "El cupon no es valido");
    // }

    // switch (cupon) {
    //     case "Raycupon":
    //         discount = 30;
    //         break;
    //     case "este_es_otro_cupon":
    //         discount = 25;
    //         break;
    //     default:
    //         return (pResult.innerText = "El cupon no es valido");
    // }

    // if (cupon == "Raycupon") {
    //     discount = 30;
    // } else if (cupon == "este_es_otro_cupon") {
    //     discount = 25;
    // } else {
    //     return (pResult.innerText = "El cupon no es valido");
    // }

    const newPrice = (precio * (100 - discount)) / 100;

    pResult.innerText = "El nuevo precio con descuento es $" + newPrice;
}
